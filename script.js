window.onload = function () {
    "use strict"
    var ecran = document.getElementById("ecran");
    var menu = document.getElementById("menu");

    switch (navigator.appVersion) {
        case "5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727; .NET CLR 3.0.30729; .NET CLR 3.5.30729; rv:11.0) like Gecko":
            menu.style.marginBottom = "133px";
            break;

        case "5.0 (compatible; MSIE 10.0; Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727; .NET CLR 3.0.30729; .NET CLR 3.5.30729)":
            menu.style.marginBottom = "133px";
            break;

        case "5.0 (compatible; MSIE 9.0; Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727; .NET CLR 3.0.30729; .NET CLR 3.5.30729)":
            menu.style.marginBottom = "133px";
            break;

        case "5.0 (Windows NT 6.2; WOW64) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2":
            menu.style.marginBottom = "123px";
            break;

        default:
            break;
    }
    var sens = "scaleX(1)";
    var sensInverse = "scaleX(-1)";
    menu.style.opacity = 0.9;
    ecran.style.opacity = 0.5;


    // Initialisation des positions du personnage jouable

    var image1 = document.getElementById("contenu");
    var image1Masque = document.getElementById("container");
    image1.style.left = "4px";
    image1.style.top = "1px";
    image1Masque.style.left = "5px";
    image1Masque.style.width = "67px";
    image1Masque.style.height = "102px";
    image1Masque.style.top = 500 - parseFloat(image1Masque.style.height) - 15 + "px";

    // Initialisation des positions du héros

    var image2 = document.getElementById("contenuSecond");
    var image2Masque = document.getElementById("containerSecond");
    image2Masque.style.transform = sensInverse;
    image2.style.left = "4px";
    image2.style.top = "1px";
    image2Masque.style.width = "67px";
    image2Masque.style.height = "107px";
    image2Masque.style.top = 500 - parseFloat((image2Masque).style.height) - 15 + "px";
    image2Masque.style.left = 1200 - parseFloat((image2Masque).style.width) + "px";

    // Initialisation des positions de la boule de feu 3

    var image3 = document.getElementById("contenuTroisieme");
    var image3Masque = document.getElementById("containerTroisieme");
    image3Masque.style.transform = sensInverse;
    image3.style.left = "-656px";
    image3.style.top = "-755px";
    image3Masque.style.width = "100px";
    image3Masque.style.height = "107px";
    image3Masque.style.left = 1200 - parseFloat((image2Masque).style.width) - parseFloat((image3Masque).style.width) + "px";
    image3Masque.style.top = 500 - parseFloat((image2Masque).style.height) - 50 + "px";

    // Initialisation des positions de la technique finale

    var image4 = document.getElementById("contenuQuatrieme");
    var image4Masque = document.getElementById("containerQuatrieme");
    image4Masque.style.transform = sensInverse;
    image4.style.left = "132px";
    image4.style.top = "-260px";
    image4Masque.style.width = "123px";
    image4Masque.style.height = "160px";
    image4Masque.style.top = 500 - parseFloat((image4Masque).style.height) - 15 + "px";
    image4Masque.style.left = 1200 - parseFloat((image4Masque).style.width) + "px";

    // Initialisation des positions des icones

    var iconeHtmlCss = document.getElementById("html-css");
    iconeHtmlCss.style.width = "50px";
    iconeHtmlCss.style.height = "50px";
    iconeHtmlCss.style.left = "250px";
    iconeHtmlCss.style.top = "427px";
    iconeHtmlCss.style.display = "block";

    var iconeJavascript = document.getElementById("javascript");
    iconeJavascript.style.width = "50px";
    iconeJavascript.style.height = "50px";
    iconeJavascript.style.left = "593px";
    iconeJavascript.style.top = "427px";
    iconeJavascript.style.display = "block";

    var iconeJquery = document.getElementById("jquery");
    iconeJquery.style.width = "50px";
    iconeJquery.style.height = "50px";
    iconeJquery.style.left = "924px";
    iconeJquery.style.top = "427px";
    iconeJquery.style.display = "block";

    // Initialisation des positions des barres de compétences

    var competenceHtmlCss = document.getElementById("competencesHtmlCss");
    competenceHtmlCss.style.width = "0px";
    var competenceJavascript = document.getElementById("competencesJavascript");
    competenceJavascript.style.width = "0px";
    var competenceJquery = document.getElementById("competencesJquery");
    competenceJquery.style.width = "0px";

    // Fixation de la barre de vie du personnage

    var javascriptBar = document.createElement("div");
    var javascriptBarContainer = document.createElement("div")
    javascriptBarContainer.appendChild(javascriptBar);
    ecran.appendChild(javascriptBarContainer);
    javascriptBar.style.margin = "0px";
    javascriptBarContainer.style.margin = "0px";
    javascriptBar.style.padding = "0px";
    javascriptBarContainer.style.padding = "0px";
    javascriptBar.style.backgroundColor = "purple";
    javascriptBarContainer.style.border = "1px solid black";
    javascriptBar.style.width = "50px";
    javascriptBarContainer.style.width = "50px";
    javascriptBar.style.height = "5px";
    javascriptBarContainer.style.height = "5px";
    javascriptBar.style.position = "absolute";
    javascriptBarContainer.style.position = "absolute";
    javascriptBarContainer.style.left = parseFloat(image1Masque.style.left) + "px";
    javascriptBarContainer.style.top = parseFloat(image1Masque.style.top) - parseFloat(image1Masque.style.height) + 80 + "px";

    // Actualisation des coordonnées de la barre de vie du personage

    var positionBarreDeVie = function () {
        javascriptBarContainer.style.left = parseFloat(image1Masque.style.left) + "px";
    };

    // On regarde en permanence si le personnage jouable n'a pas rencontré d'icones

    var detectionCollisionIcones = function () {
        if (!controlleur.lectureCollisionIconeJquery) {
            if (parseFloat(image1Masque.style.left) >= parseFloat(iconeJquery.style.left) - parseFloat(image1Masque.style.width)) {
                controlleur.lectureCollisionIconeJquery = true;
                jouer.animationGetSkills(iconeJquery, id.iconeWidthJquery, 36);
                if (!controlleur.lectureProgressionJquery) {
                    controlleur.lectureProgressionJquery = true;
                    jouer.animationSkillsBar(id.iconeJquery, competencesJquery, "#0868ab");
                }
            };
        };
        if (!controlleur.lectureCollisionJavaScript) {
            if (parseFloat(image1Masque.style.left) >= parseFloat(iconeJavascript.style.left) - parseFloat(image1Masque.style.width)) {
                controlleur.lectureCollisionJavaScript = true;
                jouer.animationGetSkills(iconeJavascript, id.iconeWidthJavascript, 23);
                if (!controlleur.lectureProgressionJavascript) {
                    controlleur.lectureProgressionJavascript = true;
                    jouer.animationSkillsBar(id.iconeJavascript, competenceJavascript, "#d6ba32");
                }
            };
        };
        if (!controlleur.lectureCollisionIconeHtmlCss) {
            if (parseFloat(image1Masque.style.left) >= parseFloat(iconeHtmlCss.style.left) - parseFloat(image1Masque.style.width)) {
                controlleur.lectureCollisionIconeHtmlCss = true;
                jouer.animationGetSkills(iconeHtmlCss, id.iconeWidthHtmlCss, 10);
                if (!controlleur.lectureProgressionHtml) {
                    controlleur.lectureProgressionHtml = true;
                    jouer.animationSkillsBar(id.iconeHtmlCss, competencesHtmlCss, "#e54d25");
                }
            };
        }
        if (controlleur.lectureCollisionIconeHtmlCss && controlleur.lectureCollisionJavaScript && controlleur.lectureCollisionIconeJquery) {
            clearInterval(id.iconesCollision);
        }
    };

    // Variable de controle des animations

    var controlleur = {
        clavierActif: false,
        garde: false,
        lecture: false,
        course: false,
        epee: false,
        lectureProgressionHtml: false,
        lectureProgressionJavascript: false,
        lectureProgressionJquery: false,
        lectureCollisionIconeHtmlCss: false,
        lectureCollisionJavaScript: false,
        lectureCollisionIconeJquery: false,
        reinitialise: function () {
            this.clavierActif = true;
            this.epee = false;
            this.garde = false;
            this.lecture = false;
            this.course = false;
            this.lectureProgressionHtml = false;
            this.lectureProgressionJavascript = false;
            this.lectureProgressionJquery = false;
            this.lectureCollisionIconeHtmlCss = false;
            this.lectureCollisionJavaScript = false;
            this.lectureCollisionIconeJquery = false;
        }
    };

    // Initialisation de tous les compteurs

    var compteur = {
        i: 0,
        m: 0,
        n: 0,
        p: 0,
        q: 0,
        r: 5,
        s: 0,
        t: 0,
        v: 0,
        reinitialise: function () {
            this.i = 0;
            this.m = 0;
            this.n = 0;
            this.p = 0;
            this.q = 0;
            this.r = 5;
            this.s = 0;
            this.t = 0;
            this.v = 0;
        }
    };

    // Décomposition des différents mouvements
    // Valeur 1 : Position left de l'image
    // Valeur 2 : Position top de l'image
    // Valeur 3 : Width du masque de l'image
    // Valeur 3 : Height du masque de l'image

    var mouvement = {
        hitsBlade: [
            ["-1px", "-105px", "67px", "102px"],
            ["-68px", "-105px", "122px", "102px"],
            ["-196px", "-105px", "122px", "102px"],
            ["-319px", "-105px", "141px", "102px"],
            ["-461px", "-105px", "101px", "102px"],
            ["-572px", "-105px", "90px", "102px"],
            ["-189px", "-350px", "134px", "111px"],
            ["-334px", "-345px", "139px", "111px"],
            ["-483px", "-345px", "139px", "111px"],
            ["-627px", "-345px", "139px", "111px"],
            ["-763px", "-345px", "121px", "111px"],
            ["-884px", "-345px", "121px", "111px"],
            ["-1005px", "-345px", "66px", "111px"],
            ["4px", "1px", "67px", "102px"],
        ],
        fireBall: [
            ["-656px", "-755px", "100px", "107px"],
            ["-3px", "-755px", "100px", "107px"],
            ["-114px", "-755px", "100px", "107px"],
            ["-224px", "-755px", "100px", "107px"],
            ["-335px", "-755px", "100px", "107px"],
            ["-445px", "-755px", "100px", "107px"],
            ["-556px", "-755px", "100px", "107px"],
            ["-656px", "-755px", "100px", "107px"]
        ],
        move: [
            ["0px", "-1005px", "67px", "102px"],
            ["-89px", "-1005px", "67px", "102px"],
            ["-191px", "-1005px", "67px", "102px"],
            ["-305px", "-1005px", "67px", "102px"],
            ["-420px", "-1005px", "67px", "102px"],
            ["-510px", "-1005px", "67px", "102px"],
            ["4px", "1px", "67px", "102px"]

        ],
        fastMove: [
            ["-88px", "-1005px", "80px", "102px"],
            ["-187px", "-1005px", "80px", "102px"],
            ["-301px", "-1005px", "80px", "102px"],
        ],
        HandUp: [
            ["4px", "1px", "67px", "107px"],
            ["-88px", "-872px", "94px", "107px"],
            ["-191px", "-872px", "94px", "107px"],
            ["-294px", "-872px", "94px", "107px"],
            ["-392px", "-872px", "94px", "107px"],
            ["4px", "-872px", "94px", "107px"],
            ["-392px", "-872px", "94px", "107px"],
            ["-294px", "-872px", "94px", "107px"],
            ["-191px", "-872px", "94px", "107px"],
            ["-88px", "-872px", "94px", "107px"],
            ["4px", "1px", "67px", "107px"],
        ],
        fastMoveHeroe: [
            ["-315px", "-750px", "67px", "107px"],
            ["-400px", "-750px", "67px", "107px"],
            ["-315px", "-750px", "67px", "107px"],
            ["-400px", "-750px", "67px", "107px"],
        ],
        evilJutsu: [
            ["4px", "1px", "67px", "107px"],
            ["6px", "-994px", "67px", "107px"],
            ["-251px", "-994px", "67px", "107px"],
            ["-344px", "-994px", "67px", "107px"],
            ["-431px", "-994px", "67px", "107px"],
            ["-519px", "-994px", "67px", "107px"],
            ["-610px", "-994px", "67px", "107px"],
            ["-697px", "-994px", "67px", "107px"],
            ["-782px", "-994px", "67px", "107px"],
            ["-890px", "-994px", "67px", "107px"],
            ["-989px", "-994px", "67px", "107px"],
            ["8px", "-117px", "67px", "107px"],
            ["-57px", "-117px", "67px", "107px"],
            ["-132px", "-117px", "67px", "107px"],
            ["-203px", "-117px", "67px", "107px"],
            ["-132px", "-117px", "67px", "107px"],
            ["-203px", "-117px", "67px", "107px"],
            ["-132px", "-117px", "67px", "107px"],
            ["-203px", "-117px", "67px", "107px"],
            ["-279px", "-117px", "67px", "107px"],
            ["-371px", "-117px", "67px", "107px"],
            ["-453px", "-117px", "67px", "107px"],
            ["-534px", "-117px", "67px", "107px"],
            ["4px", "1px", "67px", "107px"]
        ],
        afficherCv: [
            ["6px", "-994px", "67px", "107px"],
            ["-251px", "-994px", "67px", "107px"],
            ["-344px", "-994px", "67px", "107px"],
            ["-431px", "-994px", "67px", "107px"],
            ["-519px", "-994px", "67px", "107px"],
            ["-610px", "-994px", "67px", "107px"],
            ["-697px", "-994px", "67px", "107px"],
            ["-782px", "-994px", "67px", "107px"],
            ["-890px", "-994px", "67px", "107px"],
            ["-989px", "-994px", "67px", "107px"],
            ["8px", "-117px", "67px", "107px"],
            ["-57px", "-117px", "67px", "107px"],
            ["-132px", "-117px", "67px", "107px"],
            ["-203px", "-117px", "67px", "107px"],
            ["-132px", "-117px", "67px", "107px"],
            ["-203px", "-117px", "67px", "107px"],
            ["-132px", "-117px", "67px", "107px"],
            ["-203px", "-117px", "67px", "107px"],
            ["-279px", "-117px", "67px", "107px"],
            ["-611px", "-117px", "67px", "107px"],
        ],
        damages: [
            ["-75px", "-770px", "67px", "102px"],
            ["-149px", "-770px", "67px", "102px"],
            ["-220px", "-770px", "98px", "102px"],
            ["-331px", "-770px", "98px", "102px"],
            ["-431px", "-770px", "114px", "102px"],
        ],
        finalJutsu: [
            ["132px", "-260px", "123px", "160px"],
            ["-9px", "-260px", "123px", "160px"],
            ["-121px", "-260px", "123px", "160px"],
            ["-251px", "-260px", "123px", "160px"],
            ["-251px", "-260px", "123px", "160px"],
            ["-251px", "-260px", "123px", "160px"],
            ["-251px", "-260px", "123px", "160px"],
            ["-251px", "-260px", "123px", "160px"],
            ["-251px", "-260px", "123px", "160px"],
            ["-251px", "-260px", "123px", "160px"],
            ["-251px", "-260px", "123px", "160px"],
            ["-251px", "-260px", "123px", "160px"],
            ["-251px", "-260px", "123px", "160px"],
            ["-121px", "-260px", "123px", "160px"],
            ["-9px", "-260px", "123px", "160px"],
            ["132px", "-260px", "123px", "160px"]
        ],
        fuiteHeroe: [
            ["-315px", "-750px", "67px", "107px"],
            ["-400px", "-750px", "67px", "107px"],
            ["-315px", "-750px", "67px", "107px"],
            ["-400px", "-750px", "67px", "107px"],
        ],
    };

    // Identifiants des setInterval executant les fonctions

    var id = {
        afficherCv: "",
        evilJutsu: "",
        fastMove: "",
        fastMoveHeroe: "",
        finalJutsu: "",
        fireBall: "",
        fireBallExplosion: "",
        fireBallMove: "",
        hit: "",
        iconeHtmlCss: "",
        iconeJavascript: "",
        iconeJquery: "",
        iconeWidthHtmlCss: "",
        iconeWidthJavascript: "",
        iconeWidthJquery: "",
        iconesCollision: "",
        knockout: "",
        lifeLevel: "",
        move: "",
        start: "",
        fuiteHeroe: "",
        positionBarreDeVie: "",
        course: "",
    }

    // Fonctions qui animent les mouvements

    var jouer = {
        animationHitsBlade: function () {
            if (compteur.i < mouvement.hitsBlade.length) {
                image1.style.left = mouvement.hitsBlade[compteur.i][0];
                image1.style.top = mouvement.hitsBlade[compteur.i][1];
                image1Masque.style.width = mouvement.hitsBlade[compteur.i][2];
                image1Masque.style.height = mouvement.hitsBlade[compteur.i][3];
                compteur.i++;
            } else {
                compteur.i = 0;
                controlleur.lecture = false;
                controlleur.epee = false;
                clearInterval(id.hit);
            }
        },

        animationStopHitsBlade: function () {
            clearInterval(id.hit);
            compteur.i = 0;
            controlleur.lecture = false;
            controlleur.epee = false;
        },

        animationEnGarde: function () {
            if (!controlleur.garde && controlleur.clavierActif) {
                compteur.i = 0;
                controlleur.lecture = false;
                controlleur.course = false;
                clearInterval(id.move);
                clearInterval(id.hit);
                controlleur.lecture = true;
                if (image1Masque.style.transform === sensInverse) {
                    image1Masque.style.transform = sens;
                }
                controlleur.garde = true;
                image1.style.left = "4px";
                image1.style.top = "-768px";
                image1Masque.style.width = "67px";
                image1Masque.style.height = "102px";
            };
        },

        animationStopMiseEnGarde: function () {
            if (controlleur.clavierActif) {
                controlleur.garde = false;
                controlleur.lecture = false;
                controlleur.course = false;
                image1.style.left = "4px";
                image1.style.top = "1px";
                image1Masque.style.width = "67px";
                image1Masque.style.height = "102px";
            }
        },

        animationFinalJutsu: function () {
            if (compteur.m < mouvement.finalJutsu.length) {
                image4Masque.style.left = parseFloat(image1Masque.style.left) - parseFloat(parseFloat(image1Masque.width)) / 2 + "px";
                image4.style.left = mouvement.finalJutsu[compteur.m][0];
                image4.style.top = mouvement.finalJutsu[compteur.m][1];
                image4Masque.style.width = mouvement.finalJutsu[compteur.m][2];
                image4Masque.style.height = mouvement.finalJutsu[compteur.m][3];
                image4Masque.style.top = (parseFloat(500) - parseFloat((image4Masque).style.height)) - 10 + "px";
                compteur.m++;
            } else {
                compteur.m = 0;
                clearInterval(id.finalJutsu);
                setTimeout(function () {
                    id.knockout = setInterval(jouer.animationKnockOut, 200);
                    id.lifeLevel = setInterval(jouer.animationLifeLevel, 25);
                }, 500)
            }
        },

        animationFastMoveHeroe: function () {
            if (compteur.t < mouvement.fastMoveHeroe.length) {
                image2.style.left = mouvement.fastMoveHeroe[compteur.t][0];
                image2.style.top = mouvement.fastMoveHeroe[compteur.t][1];
                image2Masque.style.width = mouvement.fastMoveHeroe[compteur.t][2];
                image2Masque.style.height = mouvement.fastMoveHeroe[compteur.t][3];
                compteur.t++;
            } else {
                compteur.t = 0;
                clearInterval(id.fastMoveHeroe);
                image2Masque.style.left = parseFloat(image1Masque.style.left) - parseFloat(image1Masque.style.width) - parseFloat(image2Masque.style.width) + "px";
            }
        },

        animationFuiteHeroe: function () {
            if (compteur.t < mouvement.fuiteHeroe.length) {
                image2.style.left = mouvement.fuiteHeroe[compteur.t][0];
                image2.style.top = mouvement.fuiteHeroe[compteur.t][1];
                image2Masque.style.width = mouvement.fuiteHeroe[compteur.t][2];
                image2Masque.style.height = mouvement.fuiteHeroe[compteur.t][3];
                compteur.t++;
            } else {
                compteur.t = 0;
                clearInterval(id.fuiteHeroe);
            }
        },

        animationEvilJutsu: function () {
            if (compteur.n < mouvement.evilJutsu.length) {
                image2.style.left = mouvement.evilJutsu[compteur.n][0];
                image2.style.top = mouvement.evilJutsu[compteur.n][1];
                image2Masque.style.width = mouvement.evilJutsu[compteur.n][2];
                image2Masque.style.height = mouvement.evilJutsu[compteur.n][3];
                compteur.n++;
            } else {
                compteur.n = 0;
                clearInterval(id.evilJutsu);
                setTimeout(jouer.animationAfficherCv, 1550);
            }
        },

        animationAfficherCv: function () {
            id.afficherCv = setInterval(function () {
                if (compteur.v < mouvement.afficherCv.length) {
                    image2.style.left = mouvement.afficherCv[compteur.v][0];
                    image2.style.top = mouvement.afficherCv[compteur.v][1];
                    image2Masque.style.width = mouvement.afficherCv[compteur.v][2];
                    image2Masque.style.height = mouvement.afficherCv[compteur.v][3];
                    compteur.v++;
                } else {
                    clearInterval(id.afficherCv);
                    compteur.v = 0;
                    image2Masque.addEventListener("mouseover", function () {
                        image2Masque.style.cursor = "pointer";
                    });
                    image2Masque.addEventListener("click", function () {
                        open("./img/CV.pdf")
                    });
                };
            }, 100);
        },

        animationStopRunning: function () {
            if (controlleur.clavierActif) {
                clearInterval(id.move);
                clearInterval(id.fastMove);
                compteur.i = 0;
                controlleur.lecture = false;
                image1.style.left = "4px";
                image1.style.top = "1px";
            }
        },

        animationKnockOut: function () {
            if (compteur.s < mouvement.damages.length) {
                image1.style.left = mouvement.damages[compteur.s][0];
                image1.style.top = mouvement.damages[compteur.s][1];
                image1Masque.style.width = mouvement.damages[compteur.s][2];
                image1Masque.style.height = mouvement.damages[compteur.s][3];
                compteur.s++;
            } else {
                clearInterval(id.knockout);
                compteur.s = 0;
            }
        },

        animationLifeLevel: function () {
            if (parseFloat(javascriptBar.style.width) > 5) {
                javascriptBar.style.width = parseFloat(javascriptBar.style.width) - 1 + "px";
                if (parseFloat(javascriptBar.style.width) <= 6) {
                    javascriptBar.style.backgroundColor = "red";
                }
            } else {
                clearInterval(id.lifeLevel);
            }

        },

        animationGetSkills: function (icone, id, leftPosition) {
            if (iconeJquery.style.display === "block") {
                id = setInterval(function () {
                    if (parseFloat(icone.style.width) > 0) {
                        icone.style.width = parseFloat(icone.style.width) - 2 + "px";
                        icone.style.height = parseFloat(icone.style.height) - 2 + "px";
                        icone.style.left = parseFloat(icone.style.left) - leftPosition + "px";
                        icone.style.top = parseFloat(icone.style.top) - 17 + "px";
                    } else {
                        clearInterval(id);
                        icone.style.display = "none";
                    }
                }, 55);
            };
        },

        animationSkillsBar: function (id, skillBar, color) {
            if (parseFloat(skillBar.style.width) <= 100) {
                id = setInterval(function () {
                    if (parseFloat(skillBar.style.width) <= 100) {
                        skillBar.style.width = parseFloat(skillBar.style.width) + 1 + "px";
                        skillBar.style.backgroundColor = color;
                    } else {
                        clearInterval(id);
                    }
                }, 10);
            }
        },

        animationStartGame: function () {
            var idOpacityMenu;
            var idOpacityEcran;
            idOpacityMenu = setInterval(function () {
                if (menu.style.opacity > 0) {
                    menu.style.opacity = menu.style.opacity - 0.1;
                } else {
                    clearInterval(idOpacityMenu);
                    menu.style.display = "none";
                }
            }, 110);
            idOpacityEcran = setInterval(function () {
                if (ecran.style.opacity < 1) {
                    ecran.style.opacity = ecran.style.opacity - (-1 * 0.1);
                } else {
                    clearInterval(idOpacityEcran);
                }
            }, 220);
        },

        animationGameover: function () {
            var imageDeFin = document.createElement("div")
            imageDeFin.id = "fin";
            imageDeFin.style.width = "1200px";
            imageDeFin.style.height = "500px";
            imageDeFin.style.borderRadius = "10px";
            imageDeFin.style.backgroundImage = "url(./img/gameover.png)";
            imageDeFin.style.backgroundRepeat = "no-repeat";
            imageDeFin.style.backgroundSize = "cover";
            imageDeFin.style.marginLeft = "auto";
            imageDeFin.style.marginRight = "auto"
            imageDeFin.style.marginBottom = "0px";
            imageDeFin.style.marginTop = "0px";
            ecran.appendChild(imageDeFin);
            imageDeFin.style.opacity = 0;
            var idOpaciteGameOver;
            idOpaciteGameOver = setInterval(function () {
                if (imageDeFin.style.opacity < 1) {
                    imageDeFin.style.opacity = imageDeFin.style.opacity - (-1 * 0.1);
                } else {
                    clearInterval(idOpaciteGameOver);
                    var tentative = confirm("On retourne au combat!");
                    if (tentative) {
                        reinitialiser();
                        id.positionBarreDeVie = setInterval(positionBarreDeVie, 10);
                        setTimeout(function () {
                            id.iconesCollision = setInterval(detectionCollisionIcones, 10);
                            game();
                            controlleur.clavierActif = true;
                        }, 500);
                    } else {
                        document.location.href = "https://app-jeu-cv.herokuapp.com/"
                    }
                }
            }, 220);
        }
    };

    // Fonction qui execute le déoulement du jeu

    var game = function () {
        if (!(parseFloat(image2Masque.style.left) - parseFloat(image1Masque.style.left) + parseFloat(image1Masque.style.width) <= 250)) {
            id.start = setInterval(function () {
                if (compteur.p < mouvement.HandUp.length) {
                    image2.style.left = mouvement.HandUp[compteur.p][0];
                    image2.style.top = mouvement.HandUp[compteur.p][1];
                    image2Masque.style.width = mouvement.HandUp[compteur.p][2];
                    image2Masque.style.height = mouvement.HandUp[compteur.p][3];
                    image2Masque.style.left = (1200 - parseFloat((image2Masque).style.width)) + "px";
                    compteur.p++;
                    if (compteur.p == 2) {
                        id.fireBall = setInterval(function () {
                            if (compteur.q < mouvement.fireBall.length) {
                                image3.style.left = mouvement.fireBall[compteur.q][0];
                                image3.style.top = mouvement.fireBall[compteur.q][1];
                                image3Masque.style.width = mouvement.fireBall[compteur.q][2];
                                image3Masque.style.height = mouvement.fireBall[compteur.q][3];
                                compteur.q++;
                                if (compteur.q == 4) {
                                    clearInterval(id.fireBall);
                                    compteur.q = 0;
                                    id.fireBallMove = setInterval(function () {
                                        if (!(parseFloat(image1Masque.style.left) >= parseFloat(image3Masque.style.left))) {
                                            if (navigator.userAgent === "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0" || navigator.userAgent === "Mozilla/5.0 (Windows NT 10.0; Win64; x64; ServiceUI 14) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763" || navigator.userAgent === "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727; .NET CLR 3.0.30729; .NET CLR 3.5.30729; rv:11.0) like Gecko" || navigator.userAgent === "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763") {
                                                image3Masque.style.left = parseFloat(image3Masque.style.left) - 8 + "px";
                                            } else {
                                                image3Masque.style.left = parseFloat(image3Masque.style.left) - 2 + "px";
                                            }
                                        } else {
                                            clearInterval(id.fireBallMove);
                                            if (controlleur.garde) {
                                                id.fireBallExplosion = setInterval(function () {
                                                    if (compteur.r < mouvement.fireBall.length) {
                                                        image3.style.left = mouvement.fireBall[compteur.r][0];
                                                        image3.style.top = mouvement.fireBall[compteur.r][1];
                                                        image3Masque.style.width = mouvement.fireBall[compteur.r][2];
                                                        image3Masque.style.height = mouvement.fireBall[compteur.r][3];
                                                        compteur.r++;
                                                    } else {
                                                        clearInterval(id.fireBallExplosion);
                                                        compteur.r = 5;
                                                        compteur.p = 0;
                                                        clearInterval(id.start);
                                                        image3Masque.style.left = (1200 - parseFloat((image2Masque).style.width) - parseFloat((image3Masque).style.width)) + "px";
                                                        game();
                                                    }
                                                }, 50);
                                            } else {
                                                clearInterval(id.hit);
                                                clearInterval(id.move);
                                                clearInterval(id.positionBarreDeVie);
                                                controlleur.clavierActif = false;
                                                id.lifeLevel = setInterval(jouer.animationLifeLevel, 25);
                                                id.fireBallExplosion = setInterval(function () {
                                                    if (compteur.r < mouvement.fireBall.length) {
                                                        image3.style.left = mouvement.fireBall[compteur.r][0];
                                                        image3.style.top = mouvement.fireBall[compteur.r][1];
                                                        image3Masque.style.width = mouvement.fireBall[compteur.r][2];
                                                        image3Masque.style.height = mouvement.fireBall[compteur.r][3];
                                                        compteur.r++;
                                                    } else {
                                                        clearInterval(id.fireBallExplosion);
                                                        compteur.r = 5;
                                                        compteur.p = 0;
                                                        clearInterval(id.start);
                                                        image3Masque.style.left = (1200 - parseFloat((image2Masque).style.width) - parseFloat((image3Masque).style.width)) + "px";
                                                    }
                                                }, 50);
                                                id.knockout = setInterval(jouer.animationKnockOut, 50);
                                                setTimeout(function () {
                                                    id.fuiteHeroe = setInterval(jouer.animationFuiteHeroe, 50)
                                                    setTimeout(jouer.animationGameover, 1000);
                                                }, 1000);
                                            }
                                        }
                                    }, 1)
                                }
                            } else {
                                clearInterval(id.fireBall);
                                compteur.q = 0;
                            }
                        }, 200)
                    }
                } else {
                    clearInterval(id.start);
                    compteur.p = 0;
                }
            }, 150);
        } else {
            controlleur.clavierActif = false;
            compteur.r = 5;
            compteur.p = 0;
            clearInterval(id.fireBallExplosion);
            clearInterval(id.start);
            clearInterval(id.positionBarreDeVie);
            id.evilJutsu = setInterval(jouer.animationEvilJutsu, 133);
            id.finalJutsu = setInterval(jouer.animationFinalJutsu, 200);
            id.fastMoveHeroe = setInterval(jouer.animationFastMoveHeroe, 50);
        }
    };

    window.addEventListener("keydown", function (event) {

        // Gestion des evenements concernant les mouvements du personnage jouable

        switch (event.keyCode) {
            case 13: // Coups d'épée
                if (!controlleur.lecture && !controlleur.garde && controlleur.clavierActif) {
                    controlleur.epee = true;
                    controlleur.lecture = true;
                    if (image1Masque.style.transform === sensInverse) {
                        id.hit = setInterval(jouer.animationHitsBlade, 45);
                    } else {
                        id.hit = setInterval(jouer.animationHitsBlade, 45);
                    }
                };
                break;

            case 37: // Pas en arrière du personnage jouable
                if (controlleur.clavierActif) {
                    if (!controlleur.lecture && !controlleur.course) {
                        controlleur.lecture = true;
                        controlleur.course = true;
                        image1Masque.style.transform = sensInverse;
                        id.move = setInterval(function () {
                            if (compteur.i < mouvement.move.length) {
                                image1.style.left = mouvement.move[compteur.i][0];
                                image1.style.top = mouvement.move[compteur.i][1];
                                image1Masque.width = mouvement.move[compteur.i][2];
                                image1Masque.height = mouvement.move[compteur.i][3];
                                if (parseFloat(image1Masque.style.left) >= 0) {
                                    image1Masque.style.left = parseFloat(image1Masque.style.left) - 8 + "px";
                                }
                                compteur.i++;
                            } else {
                                compteur.i = 0;
                                controlleur.lecture = false;
                                controlleur.course = false;
                                clearInterval(id.move);
                            }
                        }, 35);
                    }
                }
                break;

            case 39: // Pas en avant du personnage jouable
                if (controlleur.clavierActif) {
                    if (!controlleur.lecture && !controlleur.course) {
                        controlleur.lecture = true;
                        controlleur.course = true;
                        image1Masque.style.transform = sens;
                        id.move = setInterval(function () {
                            if (compteur.i < mouvement.move.length) {
                                image1.style.left = mouvement.move[compteur.i][0];
                                image1.style.top = mouvement.move[compteur.i][1];
                                image1Masque.width = mouvement.move[compteur.i][2];
                                image1Masque.height = mouvement.move[compteur.i][3];
                                if (parseFloat(image1Masque.style.left) < (1200 - (2 * parseFloat(image1Masque.style.width)))) {
                                    image1Masque.style.left = parseFloat(image1Masque.style.left) + 8 + "px";
                                }
                                compteur.i++;
                            } else {
                                compteur.i = 0;
                                controlleur.lecture = false;
                                controlleur.course = false;
                                clearInterval(id.move);
                            }
                        }, 35)
                    }
                }
                break;

            case 66: // Mise en garde
                jouer.animationEnGarde();
                break;
        };
    });

    // Gestion des evenements concernant les mouvements du personnage jouable

    window.addEventListener("keyup", function (event) {
        switch (event.keyCode) {
            case 66: // Arret de la mise en garde
                jouer.animationStopMiseEnGarde();
                break;

            default:
                break;
        }
    });

    // On affiche les éléments qu'une fois tous les éléments chargés

    document.getElementsByTagName("body")[0].style.visibility = "visible";

    // 
    var reinitialiser = function () {

        // Réinitialise les positions du personnage jouable

        image1.style.left = "4px";
        image1.style.top = "1px";
        image1Masque.style.left = "5px";
        image1Masque.style.width = "67px";
        image1Masque.style.height = "102px";
        image1Masque.style.top = 500 - parseFloat(image1Masque.style.height) - 15 + "px";

        // Réinitialise les positions du héros

        image1.style.left = "4px";
        image1.style.top = "1px";
        image1Masque.style.left = "5px";
        image1Masque.style.width = "67px";
        image1Masque.style.height = "102px";
        image1Masque.style.top = 500 - parseFloat(image1Masque.style.height) - 15 + "px";

        // Réinitialise les positions de la boule de feu

        image3Masque.style.transform = sensInverse;
        image3.style.left = "-656px";
        image3.style.top = "-755px";
        image3Masque.style.width = "100px";
        image3Masque.style.height = "107px";
        image3Masque.style.left = 1200 - parseFloat((image2Masque).style.width) - parseFloat((image3Masque).style.width) + "px";
        image3Masque.style.top = 500 - parseFloat((image2Masque).style.height) - 50 + "px";

        // Réinitialise les positions de l'attaque finale

        image4Masque.style.transform = sensInverse;
        image4.style.left = "132px";
        image4.style.top = "-260px";
        image4Masque.style.width = "123px";
        image4Masque.style.height = "160px";
        image4Masque.style.top = 500 - parseFloat((image4Masque).style.height) - 15 + "px";
        image4Masque.style.left = 1200 - parseFloat((image4Masque).style.width) + "px";


        // Réinitialise les positios des icones

        iconeHtmlCss.style.width = "50px";
        iconeHtmlCss.style.height = "50px";
        iconeHtmlCss.style.left = "250px";
        iconeHtmlCss.style.top = "427px";
        iconeHtmlCss.style.display = "block";

        iconeJavascript.style.width = "50px";
        iconeJavascript.style.height = "50px";
        iconeJavascript.style.left = "593px";
        iconeJavascript.style.top = "427px";
        iconeJavascript.style.display = "block";

        iconeJquery.style.width = "50px";
        iconeJquery.style.height = "50px";
        iconeJquery.style.left = "924px";
        iconeJquery.style.top = "427px";
        iconeJquery.style.display = "block";

        // Réinitialise la barre de vie 

        javascriptBar.style.backgroundColor = "purple";
        javascriptBar.style.width = "50px";

        // Réinitialise les positions des barres de compétence

        competenceHtmlCss.style.width = "0px";
        competenceJavascript.style.width = "0px";
        competenceJquery.style.width = "0px";

        // Réinitialise les controlleurs

        controlleur.reinitialise();

        // Réinitialise les compteurs

        compteur.reinitialise();

        // Supprime le fond d'écran indiquant la fin de la partie

        ecran.removeChild(document.getElementById("fin"));
    };

    // Au click sur le bouton la partie commence

    document.getElementsByTagName("body")[0].style.visibility = "visible";

    document.getElementById("play").addEventListener("click", function () {
        jouer.animationStartGame();
        setTimeout(function () {
            id.iconesCollision = setInterval(detectionCollisionIcones, 10);
            id.positionBarreDeVie = setInterval(positionBarreDeVie, 10);
            game();
            controlleur.clavierActif = true;
        }, 1000);
    })
};